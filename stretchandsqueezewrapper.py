def stretchandsqueeze(array, n):
  """
  Stretch or Squeeze array(length>=2) to a given length n (n>=2).
  
  Warning: Keep in mind that eventhough the input array can be describing values y of f(x)=y,
  the stretchandsqueeze-function completely neglects the correspondence of y to any x-value.
  Our goal is to create newarr with a given length that roughly resembles the
  input array in its values.

  ©2021 Provided by Ekrem Altuntop
  """
  import numpy as np

  iarr = np.copy(array)
  l = len(iarr)
  if (l==n):
    return iarr
  newarr = np.array([])

  #=============================================================================
  #STRETCH
  #There is l-1 intervals inbetween data points in th input array. Calculate amount of 
  #data points are to be generated and put them into each respective interval.
  #Uniformly distribute. Remainder of newly generated points go into last interval.
  #Make sure that the interval borders are also included.
  if (l<n):
    #l-1 number of intervals
    #n-l difference in #points
    ppinterval = int((n-l)/(l-1)) #interp. points per interval, rest to be given last interval
    for i in range(l-2):#start at 1st interval till second last
      newarr = np.append(newarr, np.linspace(iarr[i], iarr[i+1], ppinterval+1)) #+1 such that starting value always in the linspace array
    #last interval
    pplastinterval = ppinterval + ((n-l)%(l-1))
    newarr = np.append(newarr, np.linspace(iarr[len(iarr)-2], iarr[len(iarr)-1], pplastinterval+2))#+2 such that last value of array always included 
  #=============================================================================
  #SQUEEZE
  #Slice input array into n groups, all values in one group get averaged and 
  #serve as values for newarr. 
  if (l>n):
    ppgroup = int(l/n) #points per group
    #points in last group computed taking the remaining elements of input array 
    for groupId in range(n-1):#first till second last group
      newarr = np.append(newarr, np.mean(iarr[groupId*ppgroup:groupId*ppgroup+ppgroup]))
    #last group (index n-1 in newarr)
    indexLast = n-1
    newarr = np.append(newarr, np.mean(iarr[indexLast*ppgroup:l]))

  return newarr

if __name__=="__main__":
  import argparse
  import numpy as np

  parser = argparse.ArgumentParser(prog="stretchandsqueeze-wrapper", description="Stretch or Squeeze array(length>=2) to a given length n (n>=2).", epilog="©2021 Provided by Ekrem Altuntop")
  parser.add_argument("inputDataOrFile")
  
  parser.add_argument("-v", "--version", help="give version details.", action="version", version="%(prog)s 1.0")
  parser.add_argument("-n", "--numelements", help="number of output elements. Default value: 2", default=2, type=int)
  parser.add_argument("-d", "--debug", help="enables debugging output.", action="store_true")
  parser.add_argument("-V", "--verbose", help="get more verbose output.", action="store_true")
  inputgroup = parser.add_mutually_exclusive_group()
  inputgroup.add_argument("-f", "--file", help="read data from an input file (single column txt-file). ", action="store_true")
  inputgroup.add_argument("--stdin", help="read input data from command line (e.g. \"[1 2.0 3. 4]\")", action="store_true")
  args = parser.parse_args()


  if args.debug:
    print("Debugging Mode")
    print("__name__:", __name__)

    print("####################")
    print("ARGUMENT VALUES:")
    print("-v, --version", args.version)
    print("-n, --numelements:", args.numelements)
    print("-d, --debug:", args.debug)
    print("-V, --verbose", args.verbose)
    print("-f, --file:", args.file)
    print("--stdin:", args.stdin)
    print("inputDataOrFile:", args.inputDataOrFile)
    print("####################")
    print("DATA VALUES:")

  if args.numelements<2:
    raise argparse.ArgumentError("--numelements requires a value of at least 2.")  
  
  if args.file:
    inputdata = np.loadtxt(args.inputDataOrFile, dtype=float)
    outputsize = args.numelements
    if args.debug:
      print("PARSED args.inputDataOrFile:", args.inputDataOrFile)
      print("inputdata for stretchandsqueeze routine:", inputdata)
      print("Proceed with stretchsqueeze routine...")
    outputarray = stretchandsqueeze(inputdata, outputsize)
  else:
    inputdata = np.asarray(args.inputDataOrFile.strip("[").strip("]").split(" "), dtype="float")
    outputsize = args.numelements
    if args.debug:
      print("PARSED args.inputDataOrFile:", args.inputDataOrFile)
      print("inputdata for stretchandsqueeze routine:", inputdata)
      print("Proceed with stretchsqueeze routine...")
    outputarray = stretchandsqueeze(inputdata, outputsize)

  
  if (args.verbose or args.debug) and len(inputdata)<args.numelements:
    print("The following output array has been achieved through the subroutine STRETCH.")
  elif (args.verbose or args.debug) and len(inputdata)>args.numelements:
    print("The following output array has been achieved through the subroutine SQUEEZE.")
  elif (args.verbose or args.debug) and len(inputdata)==args.numelements:
    print("The following output array is identical to the input array.")

  print(outputarray)
